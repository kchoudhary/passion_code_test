module Campaign::Contract
  class Create < Reform::Form
    property :title, validates: { presence: true }
    property :start_date, validates: { presence: true }
    property :budget, validates: { presence: true }
    property :brief

    validate :start_date_cannot_be_in_the_past

    def start_date_cannot_be_in_the_past
      if start_date.present? && start_date.to_date < Date.today
        errors.add(:start_date, "can't be in the past")
      end
    end 
  end
end
