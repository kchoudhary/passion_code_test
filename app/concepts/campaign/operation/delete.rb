class Campaign::Operation::Delete < Trailblazer::Operation
  step Model(Campaign, :find)
  step -> (options) { options["model"].destroy }
end
