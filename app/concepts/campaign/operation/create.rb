class Campaign::Operation::Create < Trailblazer::Operation
  step     Model( Campaign, :new )
  step Contract::Build(constant: Campaign::Contract::Create)
  step Contract::Validate()
  step Contract::Persist()
end

# class Campaign::Operation::Create < Trailblazer::Operation
#   class Contract < Reform::Form
#     property :title
#   end

#   def self.process(params)
#     campaign = Campaign.new
#     validate(params, campaign) do |contract|
#       contract.save
#       # further after_save logic happens here
#     end
#   end
# end