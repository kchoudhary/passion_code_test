class Campaign::Operation::Index < Trailblazer::Operation
  step :model

  def model(options)
    options["model"] = Campaign.all
  end
end
