class Campaign::Operation::Show < Trailblazer::Operation
  step Model(Campaign, :find)
end
