class CampaignsController < ApplicationController

  def index
    @campaigns = Campaign::Operation::Index.()["model"]
    render json: @campaigns
  end

  def show
    model = Campaign::Operation::Show.(id: params[:id])["model"]
    render json: model
  end

  def create
    res =  Campaign::Operation::Create.(campaign_params)
    if res.success?
      api_response(:created, true)
    else
      api_response(:Unprocessable, false, res["contract.default"].errors.full_messages)
    end
  end

  def update
    res = Campaign::Operation::Update.({id: params[:id]}.merge(campaign_params))
    if res.success?
      api_response(:ok, true)
    else
      api_response(:Unprocessable, false, res["contract.default"].errors.full_messages)
    end
  end

  def destroy
    res = Campaign::Operation::Delete.(id: params[:id])
    api_response(:ok, true)
  end

  private

  def campaign_params
    params.require(:campaign).permit(:title, :start_date, :budget, :brief)
  end
end
