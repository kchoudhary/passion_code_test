require 'active_support/concern'

module ExceptionHandler
  extend ActiveSupport::Concern

  def record_not_found
    render json: { error: 'Not Found' }, status: 404
  end

  def render_404
    render json: { error: 'Bad Url' }, status: 400
  end

  def raise_not_found!
    raise ActionController::RoutingError.new("No route matches #{params[:unmatched_route]}")
  end
end