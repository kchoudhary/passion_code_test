Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :campaigns
  match '*unmatched_route', :to => 'application#raise_not_found!', :via => :all
end
